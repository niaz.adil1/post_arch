#!/bin/sh

# CODE
sudo pacman -S --noconfirm code

PACKAGES=(
Tabnine.tabnine-vscode
rvest.vs-code-prettier-eslint
donjayamanne.githistory
mhutchie.git-graph
hediet.vscode-drawio
ms-python.python
ginfuru.ginfuru-better-solarized-dark-theme
)

for PACKAGE in ${PACKAGES[@]}; do
  code --install-extension $PACKAGE
done


# NVM
yay -S --noconfirm ngrok nvm
nvm install 16


# DOCKER
sudo pacman -S --noconfirm docker


# NGROK
yay -S --noconfirm ngrok
