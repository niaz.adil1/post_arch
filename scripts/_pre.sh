#!/bin/sh

# Network
sudo pacman -Syu --noconfirm
sudo pacman -S --noconfirm archlinux-keyring

# Dialog
sudo pacman -S --noconfirm dialog
cp "$BASE_DIR/.dialogrc" ~/.dialogrc

# Video Cards
VIDEO_CARDS=`lspci | grep -e VGA -e 3D`

option=$(dialog \
--title "Video Cards" \
--menu "\n$VIDEO_CARDS" 20 80 0 \
"AMD" "" \
"INTEL" "" \
"NVIDIA" "" \
"VMWARE / VIRTUALBOX" "" \
3>&1 1>&2 2>&3)

VIDEO_CARD_DRIVERS=" mesa xf86-video-vesa"
case "$option" in
  "AMD" ) VIDEO_CARD_DRIVERS="${VIDEO_CARD_DRIVERS} mesa xf86-video-amdgpu xf86-video-ati libva-mesa-driver vulkan-radeon";;
  "INTEL" ) VIDEO_CARD_DRIVERS="${VIDEO_CARD_DRIVERS} xf86-video-intel libva-intel-driver intel-media-driver vulkan-intel";;
  "NVIDIA" ) VIDEO_CARD_DRIVERS="${VIDEO_CARD_DRIVERS} xf86-video-nouveau libva-mesa-driver";;
  "VMWARE / VIRTUALBOX" ) VIDEO_CARD_DRIVERS="${VIDEO_CARD_DRIVERS} xf86-video-vmware";;
  * )	;;
esac

# Bar
STATUS_BAR=$(dialog \
--title "Video Cards" \
--menu "\nChoose a Status bar:" 20 80 0 \
"I3_BLOCKS" "" \
"POLYBAR" "" \
3>&1 1>&2 2>&3)

export VIDEO_CARD_DRIVERS
export STATUS_BAR

clear

# Super User
sudo sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g' /etc/sudoers

# Super User for Installation
sudo sh -c 'echo "Defaults lecture = never" >> /etc/sudoers.d/privacy'
sudo sed -i 's/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/g' /etc/sudoers

# Network
sudo sh -c 'echo "

127.0.0.1       localhost
::1             localhost
" >> /etc/hosts'

# Shell
sudo pacman -S --noconfirm zsh
sudo chsh -s $(which zsh) root
sudo chsh -s $(which zsh) $USER

# User Groups
sudo usermod -a -G wheel,storage,power $USER
