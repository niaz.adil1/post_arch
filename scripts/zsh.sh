#!/bin/sh

yay -S --noconfirm zsh-syntax-highlighting zsh-z-git

sudo pacman -S --noconfirm fzf zsh-theme-powerlevel10k zsh-autosuggestions
