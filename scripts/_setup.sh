#!/bin/sh

##########################################
#
# PRE
#
##########################################
/bin/sh "$BASE_DIR/scripts/_pre.sh"


##########################################
#
# SETUP
#
##########################################
# Yay Installation
/bin/sh "$BASE_DIR/scripts/yay_installation.sh"

# Packages
/bin/sh "$BASE_DIR/scripts/packages.sh"

# Video Cards
/bin/sh "$BASE_DIR/scripts/video_card.sh"

# Firewall
/bin/sh "$BASE_DIR/scripts/firewall.sh"

# Compositor
/bin/sh "$BASE_DIR/scripts/compositor.sh"

# YAY PACKAGES
/bin/sh "$BASE_DIR/scripts/yay_packages.sh"

# ANTIVIRUS
/bin/sh "$BASE_DIR/scripts/antivirus.sh"

# i3
/bin/sh "$BASE_DIR/scripts/i3.sh"

# Status Bar
/bin/sh "$BASE_DIR/scripts/status_bar.sh"

# File Manager
/bin/sh "$BASE_DIR/scripts/file_manager.sh"

# Application Launcher
/bin/sh "$BASE_DIR/scripts/app_launcher.sh"

# X
/bin/sh "$BASE_DIR/scripts/x_server.sh"

# ZSH
/bin/sh "$BASE_DIR/scripts/zsh.sh"

# VIM
/bin/sh "$BASE_DIR/scripts/vim.sh"

# FONTS
/bin/sh "$BASE_DIR/scripts/fonts.sh"

# DEV PACKAGES
/bin/sh "$BASE_DIR/scripts/dev.sh"

# THEME
/bin/sh "$BASE_DIR/scripts/_theme/setup.sh"

# DOTFILES
/bin/sh "$BASE_DIR/scripts/dotfiles.sh"

# VIM_AUTOCOMPLETION
/bin/sh "$BASE_DIR/scripts/vim_autocomplete.sh"

# VITUALIZATION
/bin/sh "$BASE_DIR/scripts/virt.sh"

# CLEANUP
/bin/sh "$BASE_DIR/scripts/_post.sh"


##########################################
#
# POST
#
##########################################
/bin/sh "$BASE_DIR/scripts/_post.sh"
