#!/bin/sh

sudo pacman -S --noconfirm clamav

sudo freshclam

curl https://secure.eicar.org/eicar.com.txt | clamscan -

yay -S --noconfirm python-fangfrisch
sudo -u clamav /usr/bin/fangfrisch --conf /etc/fangfrisch/fangfrisch.conf initdb
sudo systemctl enable fangfrisch.timer

sudo touch /var/lib/clamav/clamd.sock
chown clamav:clamav /var/lib/clamav/clamd.sock

sudo systemctl start clamav-daemon.service
sudo systemctl enable clamav-daemon.service
