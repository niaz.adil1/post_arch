#!/bin/sh

yay -Yc --noconfirm

sudo pacman -Scc --noconfirm

sudo rm /etc/sudoers.d/privacy

sudo sed -i 's/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/g' /etc/sudoers

clear
